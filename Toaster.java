public class Toaster{
	String color;
	int price;
	int maxTemp;
	
	public void toastBread(){
		System.out.println("your bread has reached " + maxTemp + " degrees celcius!");
	}
	
	public void discountPrice(){
		System.out.println("the price was " + this.price + " dollars, but the toaster doesnt work anymore so ");
		this.price = 0;
		System.out.println("now it is " + this.price + " dollars");
	}
}