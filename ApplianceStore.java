import java.util.Scanner;

class ApplianceStore{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		Toaster[] toasterList = new Toaster[4];
		
		for (int i = 0; i < 4; i++){
			toasterList[i] = new Toaster();
			System.out.println("type price of toaster");
			toasterList[i].price = keyboard.nextInt();
			System.out.println("type color of toaster");
			keyboard.nextLine();
			toasterList[i].color = keyboard.nextLine();
			System.out.println("type the max temperature of the toaster");
			toasterList[i].maxTemp = keyboard.nextInt();
		}
		System.out.println(toasterList[3].color + " " + toasterList[3].maxTemp + " " + toasterList[3].price);
		toasterList[0].toastBread();
		toasterList[0].discountPrice();
	}
}